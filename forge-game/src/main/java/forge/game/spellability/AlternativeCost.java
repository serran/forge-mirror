package forge.game.spellability;

public enum AlternativeCost {
    Awaken,
    Bestow,
    Cycling, // ActivatedAbility
    Dash,
    Emerge,
    Escape,
    Evoke,
    Flashback,
    Foretold,
    Madness,
    Mutate,
    Offering,
    Outlast, // ActivatedAbility
    Prowl,
    Spectacle,
    Surge;

}
