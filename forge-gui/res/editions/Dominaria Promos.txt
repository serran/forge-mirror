[metadata]
Code=PDOM
Date=2018-04-27
Name=Dominaria Promos
Type=Promos

[cards]
33 U Serra Angel
60 C Opt
76 R Zahid, Djinn of the Lamp
81 U Cast Down
168 C Llanowar Elves
182 R Steel Leaf Champion
204 U Shanna, Sisay's Legacy
249 U Zhalfirin Void
