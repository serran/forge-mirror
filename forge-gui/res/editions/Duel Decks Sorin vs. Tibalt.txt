[metadata]
Code=DDK
Date=2013-03-15
Name=Duel Decks: Sorin vs. Tibalt
MciCode=ddk
Type=Duel_Decks

[cards]
1 M Sorin, Lord of Innistrad
2 C Doomed Traveler
3 C Vampire Lacerator
4 U Wall of Omens
5 C Child of Night
6 C Duskhunter Bat
7 C Mesmeric Fiend
8 U Gatekeeper of Malakir
9 R Twilight Drover
10 C Bloodrage Vampire
11 U Fiend Hunter
12 U Vampire Nighthawk
13 U Mausoleum Guard
14 U Phantom General
15 U Vampire Outcasts
16 U Revenant Patriarch
17 U Sengir Vampire
18 R Butcher of Malakir
19 C Vampire's Bite
20 U Decompose
21 C Sorin's Thirst
22 U Urge to Feed
23 U Zealous Persecution
24 U Lingering Souls
25 U Mortify
26 U Spectral Procession
27 C Unmake
28 R Ancient Craving
29 C Mark of the Vampire
30 R Field of Souls
31 C Absorb Vis
32 R Death Grasp
33 C Evolving Wilds
34 U Tainted Field
35 L Swamp
36 L Swamp
37 L Swamp
38 L Plains
39 L Plains
40 L Plains
41 M Tibalt, the Fiend-Blooded
42 C Goblin Arsonist
43 U Scorched Rusalka
44 U Reassembling Skeleton
45 C Ashmouth Hound
46 U Hellspark Elemental
47 C Vithian Stinger
48 U Shambling Remains
49 C Coal Stoker
50 R Lavaborn Muse
51 C Mad Prophet
52 R Hellrider
53 U Skirsdag Cultist
54 U Corpse Connoisseur
55 U Scourge Devil
56 U Gang of Devils
57 C Bump in the Night
58 C Blazing Salvo
59 C Faithless Looting
60 C Flame Slash
61 C Geistflame
62 U Pyroclasm
63 U Recoup
64 C Terminate
65 C Strangling Soot
66 U Browbeat
67 R Breaking Point
68 R Sulfuric Vortex
69 C Blightning
70 U Flame Javelin
71 U Torrent of Souls
72 R Devil's Play
73 U Akoum Refuge
74 C Rakdos Carnarium
75 L Mountain
76 L Mountain
77 L Mountain
78 L Swamp
79 L Swamp
80 L Swamp

[tokens]
w_1_1_spirit_flying
b_1_1_vampire_lifelink