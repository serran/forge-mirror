[metadata]
Code=HA4
Date=2021-03-11
Name=Historic Anthology 4
Type=Online

[cards]
1 R Adorned Pouncer
2 R Declaration in Stone
3 C Thraben Inspector
4 M Triumphant Reckoning
5 C Iceberg Cancrix
6 R Marit Lage's Slumber
7 C Think Twice
8 R Ammit Eternal
9 R Death's Shadow
10 U Faith of the Devoted
11 U Torment of Scarabs
12 U Flameblade Adept
13 C Goblin Gaveleer
14 R Harmless Offering
15 C Lys Alana Huntmaster
16 R Sawtusk Demolisher
17 U Spider Spawning
18 U Hamza, Guardian of Arashin
19 R Collected Conjuring
20 U Abomination of Llanowar
21 C Bonesplitter
22 U Coldsteel Heart
23 R Inspiring Statuary
24 M Sword of Body and Mind
25 R Blinkmoth Nexus 
